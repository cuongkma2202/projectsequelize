"use strict";

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addConstraint("books", {
            fields: ["UserId"],
            type: "foreign key",
            name: "book_user_association",
            references: {
                table: "users",
                field: "id",
            },
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint("books", "book_user_association");
    },
};