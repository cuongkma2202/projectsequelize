const Sequelize = require("sequelize");
const database = require("../config/db/connectToDb");
const User = require("./User");
const Book = database.define("book", {
    // Model attributes are defined here
    id: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
    },
    userID: {
        type: Sequelize.INTEGER(11),
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    description: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    author: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    image: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});
User.hasMany(Book);
Book.belongsTo(User, { through: "userID" });
module.exports = Book;