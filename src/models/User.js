const Sequelize = require("sequelize");
const database = require("../config/db/connectToDb");
const User = database.define("user", {
    // Model attributes are defined here
    id: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
    },
    username: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});
module.exports = User;