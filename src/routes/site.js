const express = require("express");
const router = express.Router();
const siteController = require("../controllers/SiteController");
const authController = require("../controllers/AuthController");
// router.get("/book", authController.isLoggingIn, siteController.book);
router.get("/profile", authController.isLoggingIn, siteController.profile);
router.get("/register", siteController.register);
router.get("/login", siteController.login);
router.get("/", authController.isLoggingIn, siteController.index);

module.exports = router;