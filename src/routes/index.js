const siteRoute = require("./site");
const authRoute = require("./auth");
const bookRoute = require("./book");
const route = (app) => {
    app.use("/book", bookRoute);
    app.use("/auth", authRoute);
    app.use("/", siteRoute);
    app.use(function(req, res) {
        res.status(404).render("not_found");
    });
};
module.exports = route;