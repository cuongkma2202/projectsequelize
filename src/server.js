const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const viewEngine = require("./config/viewEngine");
const methodOverride = require("method-override");
const path = require("path");
const route = require("./routes");
require("dotenv").config();

// initial app
let app = express();
// static file
app.use(express.static(path.join(__dirname, "public")));
// config app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
// method overide
app.use(methodOverride("_method"));

viewEngine(app);
// DB Connection
// require("./config/db/connectToDb");
// init Route
route(app);
let PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
    console.log(`APP RUNNING ON PORT ${PORT}`);
});