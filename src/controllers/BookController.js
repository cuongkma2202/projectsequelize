const Book = require("../models/Book");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

class BookController {
    // Get all book
    async getAllBook(req, res) {
            try {
                if (req.user) {
                    const { userId, userEmail, userName } = req.user;
                    const books = await Book.findAll({
                        raw: true,
                        where: {
                            userId: userId,
                        },
                    });

                    await res.render("book", {
                        userId,
                        userEmail,
                        userName,
                        books,
                    });
                } else {
                    res.redirect("/login");
                }
            } catch (error) {
                console.log(error);
            }
        }
        // Get detail book
    async detailBook(req, res) {
            try {
                if (req.user) {
                    const { userId, userEmail, userName } = req.user;
                    const { id } = req.params;
                    const book = await Book.findOne({
                        raw: true,
                        where: {
                            userId: userId,
                            id: id,
                        },
                    });
                    console.log(book);

                    await res.render("detail_book", {
                        userId,
                        userEmail,
                        userName,
                        book,
                    });
                } else {
                    res.redirect("/login");
                }
            } catch (error) {
                console.log(error);
            }
        }
        // create Book
    async createBook(req, res) {
            try {
                const { title, description, author, image } = req.body;
                const { userId } = req.user;
                console.log(req.user);
                await Book.create({
                    title: title,
                    userId: userId,
                    description: description,
                    author: author,
                    image: image,
                });
                return res.redirect("/book");
            } catch (error) {
                console.log(error);
            }
        }
        // edit book
    async editBook(req, res) {
        try {
            if (req.user) {
                const { userId, userEmail, userName } = req.user;
                const { id } = req.params;
                const book = await Book.findOne({
                    raw: true,
                    where: {
                        userId: userId,
                        id: id,
                    },
                });
                await res.render("edit", {
                    userId,
                    userEmail,
                    userName,
                    book,
                });
            } else {
                res.redirect("/login");
            }
        } catch (error) {
            console.log(error);
        }
    }
    async update(req, res) {
            const { title, description, author, image } = req.body;
            const { id } = req.params;
            await Book.update({ title: title, description: description, author: author, image: image }, {
                where: {
                    id: id,
                },
            });
            return res.redirect("/book");
        }
        // delete book
    async deleteBook(req, res) {
        const { id } = req.params;
        console.log(id);
        await Book.destroy({
            where: {
                id: id,
            },
        });
        return res.redirect("/book");
    }
    async search(req, res) {
        try {
            if (req.user) {
                const { book } = req.query;
                const { userId, userEmail, userName } = req.user;
                const books = await Book.findAll({
                    raw: true,
                    where: {
                        [Op.or]: [{
                                title: {
                                    [Op.like]: "%" + book + "%",
                                },
                            },
                            {
                                author: {
                                    [Op.like]: "%" + book + "%",
                                },
                            },
                        ],
                    },
                });
                await res.render("book", {
                    userId,
                    userEmail,
                    userName,
                    books,
                });
            }
        } catch (err) {
            console.log(err);
        }
    }
}
module.exports = new BookController();