const User = require("../models/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { promisify } = require("util");
class AuthController {
    async register(req, res) {
        const { username, email, password } = req.body;
        try {
            const result = await User.findOne({
                where: {
                    email,
                },
            });
            if (result) {
                return res.render("register", {
                    message: "This email is already in use!",
                });
            }
            const hashedPassword = await bcrypt.hash(password, 10);
            await User.create({
                username: username,
                email: email,
                password: hashedPassword,
            });
            return res.render("register", {
                success: "User Registered",
            });
        } catch (err) {
            console.log(err);
        }
    }
    async login(req, res) {
        try {
            const { useremail, userpassword } = req.body;
            if (!useremail || !userpassword) {
                return res.render("login", {
                    message: "Please provide an email and password",
                });
            }
            const user = await User.findOne({
                where: {
                    email: useremail,
                },
            });
            console.log(user);
            const { id, email, password } = user;
            const checkPassword = await bcrypt.compare(userpassword, password);
            if (!user || !checkPassword) {
                return res.render("login", {
                    message: "Email or password is incorrect",
                });
            }
            const token = jwt.sign({ id, email }, process.env.ACCESS_TOKEN_SECRET, {
                expiresIn: process.env.TOKEN_EXPIRES,
            });
            console.log("token", token);

            const cookieOptions = {
                expires: new Date(
                    Date.now() + process.env.COOKIE_EXPIRES * 24 * 60 * 60 * 1000
                ),
                httpOnly: true,
            };

            res.cookie("login_book", token, cookieOptions);
            res.status(200).redirect("/");
        } catch (error) {
            console.log(error);
        }
    }
    async isLoggingIn(req, res, next) {
        console.log(req.cookies);
        if (req.cookies.login_book) {
            try {
                const decoded = await promisify(jwt.verify)(
                    req.cookies.login_book,
                    process.env.ACCESS_TOKEN_SECRET
                );
                const { id } = decoded;
                const result = await User.findOne({
                    where: {
                        id,
                    },
                });
                if (!result) {
                    return next();
                }
                const userId = result.dataValues.id;
                const userName = result.dataValues.username;
                const userEmail = result.dataValues.email;
                req.user = { userId, userEmail, userName };

                return next();
            } catch (error) {
                console.log(error);
                return next();
            }
        } else {
            next();
        }
    }
    async logout(req, res) {
        res.cookie("login_book", "logout", {
            expires: new Date(Date.now()),
            httpOnly: true,
        });
        res.redirect("/");
    }
}
module.exports = new AuthController();