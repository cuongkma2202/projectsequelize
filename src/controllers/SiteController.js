class AuthenController {
    index(req, res) {
        if (req.user) {
            const { userId, userEmail, userName } = req.user;
            res.render("home", {
                userId,
                userEmail,
                userName,
            });
        } else {
            res.render("home");
        }
    }
    register(req, res) {
        res.render("register");
    }
    login(req, res) {
        res.render("login");
    }
    profile(req, res) {
            if (req.user) {
                const { userId, userEmail, userName } = req.user;
                res.render("profile", {
                    userId,
                    userEmail,
                    userName,
                });
            } else {
                res.redirect("/login");
            }
        }
        // book(req, res) {
        //     if (req.user) {
        //         const { userId, userEmail, userName } = req.user;
        //         res.render("book", {
        //             userId,
        //             userEmail,
        //             userName,
        //         });
        //     } else {
        //         res.redirect("/login");
        //     }
        // }
}
module.exports = new AuthenController();