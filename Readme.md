# Book Management App

## _Author: Cuong Nguyen_

## Features

- Login Register
- Create Read Update Delete Book

## Tech

Book managementuses a number of open source projects to work properly:

- Sequelize - Sequelize is a promise-based Node.js ORM
- Bootstrap - great UI boilerplate for modern web apps
- NODEJS - evented I/O for the backend
- Express - fast node.js network app framework

## Installation

Install the dependencies and devDependencies and start the server.

```sh
npm install
npm start
```
